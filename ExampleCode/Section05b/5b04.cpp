/*******************************
 * File name: 5b04.cpp
 * Author: Matthew Morrison
 * E-mail: matt.morrison@nd.edu 
 * 
 * This file contains the main classes
 * for the Towers of Hanoi problem.
 * ****************************/

#include "Stack.h"
#include<iostream>

/*********************************************
 * Function Name: hanoi
 * Preconditions: size_t, Tower*, Tower*, Tower* size_t
 * Postconditions: none 
 * This function contains the Towers of Hanoi algorithm 
 * ******************************************/ 
template<class T>
void hanoi(size_t n, Tower<T> *source, Tower<T> *destination, Tower<T> *auxillary, size_t *numMoves){
    if (n > 0 ){
        hanoi(n - 1, source, auxillary, destination, numMoves);   
        move(source, destination, numMoves);               
        hanoi(n - 1, auxillary, destination, source, numMoves);     
    }

}

/*********************************************
 * Function Name: hanoi
 * Preconditions: Tower*, Tower*, Tower* size_t
 * Postconditions: none 
 * This function pops the top element off the 
 * source stack, puts in on the top of the 
 * destination stack, and prints the move to 
 * the user 
 * ******************************************/ 
template<class T>
void move(Tower<T> *source, Tower<T> *destination, size_t *numMoves){
    (*numMoves)++;
    std::cout   << "Move " << *numMoves << ": Weight #" << source->top() << " from "
                << source->getTowerNum() << " to " 
                << destination->getTowerNum() << std::endl;
                
     destination->push(source->top());
     source->pop();
}

/*********************************************
 * Function Name: main
 * Preconditions: int, argc**
 * Postconditions: none 
 * This function is the main driver function 
 * ******************************************/ 
int main(int argc, char**argv){
    
    // Total Number of Plates
    size_t STACK_SIZE = 4;
    
    // Keep track of the number of moves 
    size_t numMoves = 0;
    
    // Create three stacks for the three rods
    Tower<size_t>* tower_1 = new Tower<size_t>(STACK_SIZE, 1);
    Tower<size_t>* tower_2 = new Tower<size_t>(STACK_SIZE, 2);
    Tower<size_t>* tower_3 = new Tower<size_t>(STACK_SIZE, 3);
    
    // Put the initial plates on the first tower
    for(size_t i = STACK_SIZE; i > 0; --i){
        tower_1->push(i);
    }
    
    // Run the Hanoi algorithm
    hanoi(STACK_SIZE, tower_1, tower_2, tower_3, &numMoves);
    
    return 0;
}
