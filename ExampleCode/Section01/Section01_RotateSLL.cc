/*************************************
 * File name: Section01_RotateSLL.cc 
 * Author: Matthew Morrison
 * Email: matt.morrison@nd.edu 
 * 
 * Given a set of inputs from the user, perform a 
 * set of rotations on a array of integers
 * 
 * This example is for a Singularly LinkedList only
 * Therefore, can't go left.
 * ***********************************/

#include <iostream>
#include "Section01_LinkedList_NodeStruct.h"

const int INPUT_THROW = 100;

/*************************
 * Function Name: getInputConstr
 * Preconditions: int*, int*, char *
 * Postconditions: none 
 * Gets input from the user and determines the 
 * appropriate outputs. 
 * **********************/
void getInputConstr(int *length, int *rotations, char *cDirection){

    std::cout << "-----------------------------------------------------------------------" << std::endl;
    std::cout << "Enter the length, number of rotations, and the direction (L/R)." << std::endl;
    std::cout << "e.g.: length = 5, number of rotations = 4, and the direction is left..." << std::endl;
    std::cout << "Then the desired input is: 5 4 L" << std::endl;
    std::cout << "Enter your values: ";
    std::cin >> *length;
    
    // Check if it is an integer and if the length is greater than 0
    if(!(std::cin.good() && *length > 0)){
        std::cerr << "Incorrect first input. Must be an integer greater than 0." << std::endl;
        // Throw to the catch block in main 
        throw INPUT_THROW;
    }
    
    // Check if it is an integer and if the length is greater than 0
    std::cin >> *rotations;
    if(!(std::cin.good() && *length >= 0)){
        std::cerr << "Incorrect second input. Must be an integer >= 0." << std::endl;
        // Throw to the catch block in main 
        throw INPUT_THROW;
    }
    
    std::cin >> *cDirection;
    if(!(std::cin.good() && (*cDirection == 'R' || *cDirection == 'L'))){
        std::cerr << "Incorrect third input. Must be R or L." << std::endl;
        // Throw to the catch block in main 
        throw INPUT_THROW;
    }
}

/*************************
 * Function Name: getInputs
 * Preconditions: LinkedList<T>* myList, const int length
 * Postconditions: none 
 * Function reads in a set of inputs from the user, and - if 
 * they are of T* 
 * **********************/
template<class T>
void getInputs(LinkedList<T>* myList, const int length){
    std::cout << "Enter " << length << " integers: ";
    
    for(int i = 0; i < length; i++){
        
        // Initialize the template
        T temp;
        
        // Read in the variable 
        std::cin >> temp;
        
        // Since it is a template, use cin.good() to determine if the input was correct 
        if(!std::cin.good()){
            std::cerr << "Input " << i << " is incorrect. Must be an integer." << std::endl;
            
            // Throw to the catch block in main 
            throw INPUT_THROW;
        }
        
        // Insert temp into the list 
        myList->insert(temp);
    }
}

/******************************
 * Function Name: clearSTDCIN
 * Preconditions: none 
 * Postconditions: none
 * 
 * Clears the std::cin buffer in the event 
 * that the user wishes to after an error.
 * ****************************/
void clearSTDCIN(){
	std::cin.clear();
    std::cin.ignore(200, '\n');
}

/*************************
 * Function Name: main 
 * Preconditions: none 
 * Postconditions: int 
 * This is the main driver function 
 * **********************/
int main()
{
    char loop = 'y';
    
    while(loop == 'y'){
        try{
            // Create the LinkedList 
            LinkedList<int>* myList = new LinkedList<int>();
            
            // Set the Initial input variables
            int length, rotations;
            char cDirection;
            
            // Get the input constraints
            getInputConstr(&length, &rotations, &cDirection);
            
            // Get the inputs and add them to the array 
            getInputs(myList, length);
            
            // Output the initial array 
            std::cout << "Initial Array is: " << myList << std::endl;
            
            // If the output 
            if(cDirection == 'L')
                rotateLeft(myList, rotations % myList->getLength());
            else{ // Error checking already proved this must be 'R'
                std::cout << "Singly Linked Lists can only rotate left" << std::endl;
				rotations = 0;
            }
            
            std::cout << "After " << rotations << " rotations to the " << cDirection << " the Array is: " << myList << std::endl;
            
            delete myList;
        } catch(int INPUT_THROW){
            std::cout << "Exception caught" << std::endl;

            // Clear the cin input buffer
            clearSTDCIN();
        }
        
        std::cout << "Continue? Enter y/n: ";
        std::cin >> loop;
        
        while(!(std::cin.good() && (loop == 'y' || loop == 'n'))){
            std::cerr << "Incorrect. Must be y or n." << std::endl;
            // Clear the cin input buffer
            clearSTDCIN();
            std::cout << "Continue? Enter y/n: ";
            std::cin >> loop;
        }
    }
    
    return 0;
}